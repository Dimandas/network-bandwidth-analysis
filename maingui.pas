﻿unit MainGUI;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus,
  StdCtrls, ComCtrls, Grids, ExtCtrls, GraphGUI, ViewGraphGUI,
  fpvtocanvasM, CreateGraph, FPimage, DefinesConst;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button14: TButton;
    Button15: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    GroupBox2: TGroupBox;
    Image1: TImage;
    Image4: TImage;
    MainMenu1: TMainMenu;
    Memo1: TMemo;
    MenuItem1: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    OpenDialog1: TOpenDialog;
    PageControl1: TPageControl;
    SaveDialog1: TSaveDialog;
    StringGrid1: TStringGrid;
    StringGrid4: TStringGrid;
    TabSheet1: TTabSheet;
    TabSheet4: TTabSheet;
    procedure Button2Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    function CallWarningMessage: boolean;
    procedure ClearAllData;
    function CheckConnection(FirstNode, SecondNode: integer): integer;
    procedure FindingOfBandwidth;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure Rank;
    procedure InsertionSort;
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure CallHelp;
    procedure CallGraphDraw;
    procedure DrawPreviewGraph;
    procedure ClearPreviewGraph;
    procedure FillTable;
    procedure ClearTable;
    procedure MarkingTable;
    procedure CallViewGraph;
    function FileOpenBNF(Path: WideString): boolean;
    function FileSaveAsBNF(Path: WideString): boolean;
    procedure CallFileOpenDialog;
    procedure CallFileSaveAsDialog;
    procedure CanvasChanged;
    procedure MenuItem10Click(Sender: TObject);
    procedure MenuItem7Click(Sender: TObject);
    procedure MenuItem8Click(Sender: TObject);
    procedure MenuItem9Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

  FnData = record
    Sum: integer;
    IndexNodes: array of integer;
  end;

var
  Form1: TForm1;
  Form3: TForm3;
  Form5: TForm5;

implementation

var
  FDataList: TStringList;
  FData: array of FnData;
  TNodes: array of integer;
  GSavePath: WideString = '';
  MCountCircle: integer;

{$R *.lfm}

{ TForm1 }

function TForm1.CallWarningMessage: boolean;
var
  Select: integer;
begin
  if CanvaIsChangedFile = True then
  begin
    Select := MessageDlg(Application.Title,
      'Вы хотите сохранить данные ?', mtConfirmation,
      mbYesNoCancel, 0);
    if (Select = mrNo) then
    begin
      Result := True;
      Exit;
    end;
    if (Select = mrCancel) then
    begin
      Result := False;
      Exit;
    end;
    if (Select = mrYes) then
    begin
      if (GSavePath <> '') then
      begin
        if (FileSaveAsBNF(GSavePath)) = False then
        begin
          MessageDlg(Application.Title,
            'Не удалось сохранить файл.',
            mtError, [mbOK], 0);
          Result := False;
          Exit;
        end;
      end
      else
      if (SaveDialog1.Execute = True) then
      begin
        if (FileSaveAsBNF(SaveDialog1.FileName)) = False then
        begin
          MessageDlg(Application.Title,
            'Не удалось сохранить файл.',
            mtError, [mbOK], 0);
          Result := False;
          Exit;
        end;
        GSavePath := '';
        Result := True;
        Exit;
      end
      else
      begin
        Result := False;
        Exit;
      end;
    end;
  end;
  Result := True;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  CallFileOpenDialog;
end;

procedure TForm1.Button8Click(Sender: TObject);
begin
  CallFileOpenDialog;
end;

procedure TForm1.ClearAllData;
begin
  if Assigned(Form3) then
    Form3.FreeCircleAndLineAndClear;
  if Assigned(Form3) then
    Form3.ImageClear;
  ClearTable;
  ClearPreviewGraph;
  Memo1.Clear;
  CanvasChanged;
  CanvaIsChangedFile := False;
  GSavePath := '';
end;

procedure TForm1.InsertionSort;
var
  i, i1, n, b: integer;
  Si: integer = 0;
  temp: FnData;
begin
  n := Length(FData) - 1;
  for i := 0 to n do
  begin
    if i > Si then
    begin
      temp := FData[i];
      b := FData[i].Sum;
      for i1 := Si downto 0 do
      begin
        if b > FData[i1].Sum then
        begin
          FData[i1 + 1] := FData[i1];
          if i1 = 0 then
            FData[i1] := temp;
        end
        else
        begin
          FData[i1 + 1] := temp;
          Break;
        end;
      end;
      Si := Si + 1;
    end;
  end;
end;

procedure TForm1.Rank;
var
  TemString: TStringList;
  TempStr: string;
  AverageCount, i, i2: integer;
begin
  if Length(FData) = 0 then
  begin
    Memo1.Text := 'Нет данных для вывода.';
    Exit;
  end;
  InsertionSort;
  TemString := TStringList.Create;
  TemString.Add('Расчет пропускной способности сети:');
  AverageCount := (MCountCircle div 2);
  for i := 0 to Length(FData) - 1 do
  begin
    for i2 := 0 to MCountCircle - 1 do
    begin
      if i2 = AverageCount then
        TempStr := TempStr + ' |';
      TempStr := TempStr + ' ' + IntToStr(FData[i].IndexNodes[i2]);
    end;
    TempStr := TempStr + ' - Пропускная способность = ' +
      IntToStr(FData[i].Sum);
    TemString.Add(TempStr);
    TempStr := '';
  end;
  Memo1.Text := TemString.Text;
  FreeAndNil(TemString);
end;

function TForm1.CheckConnection(FirstNode, SecondNode: integer): integer;
var
  i: integer;
begin
  for i := 0 to Circls[FirstNode].CountConnectedLines - 1 do
  begin
    Application.ProcessMessages;
    if (Lines[Circls[FirstNode].ConnectedLinesIds[i]].IdEndCircle = SecondNode) or
      (Lines[Circls[FirstNode].ConnectedLinesIds[i]].IdStartCircle = SecondNode) then
    begin
      Result := Lines[Circls[FirstNode].ConnectedLinesIds[i]].Data3;
      Exit;
    end;
  end;
  Result := -1;
end;

procedure TForm1.FindingOfBandwidth;
var
  AverageCount, temp, i, i1, i2, i3, i4, i5, i6, i7, i8, i9, TI, Ch,
  AllVariantsTrue, CLeft, CRight, MaxBandwidth, MinBandwidth, CoFData,
  StartPOS, i10, i11, TI2: integer;
  Sum: integer = 0;
  TempStr: string = '';
  MaxBandwidthNodes: array of integer;
  MinBandwidthNodes: array of integer;
  IsMoving: boolean = False;
  Stop: boolean = False;
  Costil: byte = 0;
  OneChangeTNodes: boolean;
  CheckNodes: array of integer;
begin
  SetLength(FData, 0);
  CoFData := 0;
  MCountCircle := 0;
  Ch := 0;
  for i5 := 0 to High(Circls) do
    if Circls[i5] <> nil then
      if Circls[i5].CountConnectedLines <> 0 then
        Ch := Ch + 1;
  MCountCircle := Ch;
  MaxBandwidth := Low(MaxBandwidth);
  MinBandwidth := High(MinBandwidth);
  FDataList := TStringList.Create;
  SetLength(TNodes, MCountCircle);
  SetLength(MaxBandwidthNodes, MCountCircle);
  SetLength(MinBandwidthNodes, MCountCircle);
  SetLength(CheckNodes, MCountCircle);
  for i := 0 to MCountCircle - 1 do
    TNodes[i] := i;
  AverageCount := (MCountCircle div 2);
  CLeft := AverageCount;
  CRight := MCountCircle - AverageCount;
  StartPOS := 0;
  if (AverageCount = CRight) then
    StartPOS := StartPOS + 1;
  AllVariantsTrue := 0;
  TI2 := 0;
  Memo1.Clear;
  Memo1.Text := 'Вычисление пропускной способности сети...';
  FDataList.Add('Расчет пропускной способности сети:');
  FDataList.Add('Всего бисекций: ');
  while Stop = False do
  begin
    if Length(TNodes) = 0 then
    begin
      Stop := True;
      Continue;
    end;
    for i7 := 0 to High(CheckNodes) do
      CheckNodes[i7] := -1;
    CheckNodes[TNodes[0]] := TNodes[0];
    for i6 := 0 to Length(CheckNodes) + 1 do
      for i7 := 0 to High(CheckNodes) do
      begin
        if CheckNodes[i7] = -1 then
          Continue;
        for i8 := 0 to CLeft - 1 do
        begin
          Application.ProcessMessages;
          if (CheckNodes[i7] = TNodes[i8]) then
            Continue;
          temp := CheckConnection(CheckNodes[i7], TNodes[i8]);
          if temp <> -1 then
          begin
            CheckNodes[TNodes[i8]] := TNodes[i8];
          end;
        end;
      end;
    for i7 := 0 to CLeft - 1 do
    begin
      if TNodes[i7] <> CheckNodes[TNodes[i7]] then
        IsMoving := True;
    end;
    if IsMoving = False then
    begin
      for i7 := 0 to High(CheckNodes) do
        CheckNodes[i7] := -1;
      CheckNodes[TNodes[AverageCount]] := TNodes[AverageCount];
      for i6 := 0 to Length(CheckNodes) + 1 do
        for i7 := 0 to High(CheckNodes) do
        begin
          if CheckNodes[i7] = -1 then
            Continue;
          for i8 := AverageCount to MCountCircle - 1 do
          begin
            Application.ProcessMessages;
            if (CheckNodes[i7] = TNodes[i8]) then
              Continue;
            temp := CheckConnection(CheckNodes[i7], TNodes[i8]);
            if temp <> -1 then
            begin
              CheckNodes[TNodes[i8]] := TNodes[i8];
            end;
          end;
        end;
      for i7 := AverageCount to MCountCircle - 1 do
      begin
        if TNodes[i7] <> CheckNodes[TNodes[i7]] then
          IsMoving := True;
      end;
    end;
    if IsMoving = False then
    begin
      Application.ProcessMessages;
      AllVariantsTrue := AllVariantsTrue + 1;
      Sum := 0;
      for i1 := 0 to CLeft - 1 do
        for i2 := 0 to CRight - 1 do
        begin
          Application.ProcessMessages;
          temp := CheckConnection(TNodes[i1], TNodes[CLeft + i2]);
          if temp <> -1 then
            Sum := Sum + temp;
        end;
      if (Sum <> 0) then
      begin
        SetLength(FData, Length(FData) + 1);
        FData[CoFData].Sum := Sum;
        if (MaxBandwidth < Sum) then
        begin
          MaxBandwidth := Sum;
          for i4 := 0 to MCountCircle - 1 do
            MaxBandwidthNodes[i4] := TNodes[i4] + 1;
        end;
        if (MinBandwidth > Sum) then
        begin
          MinBandwidth := Sum;
          for i4 := 0 to MCountCircle - 1 do
            MinBandwidthNodes[i4] := TNodes[i4] + 1;
        end;
        SetLength(FData[CoFData].IndexNodes, MCountCircle);
        for i4 := 0 to MCountCircle - 1 do
        begin
          FData[CoFData].IndexNodes[i4] := TNodes[i4] + 1;
          if i4 = AverageCount then
            TempStr := TempStr + ' |';
          TempStr := TempStr + ' ' + (IntToStr(TNodes[i4] + 1));
        end;
        TempStr := TempStr + ' - Пропускная способность = ' +
          IntToStr(Sum);
        CoFData := CoFData + 1;
        FDataList.Add(TempStr);
        TempStr := '';
      end;
      if TNodes[CLeft - 1] + 1 > Length(TNodes) - 1 then
      begin
        try
          if TNodes[CLeft - 1 - TI2] + 1 > Length(TNodes) - 1 - TI2 then
          begin
            if CLeft - 1 - TI2 <= StartPOS then
            begin
              Stop := True;
              raise Exception.Create('');
            end;
            TI2 := TI2 + 1;
          end;
          OneChangeTNodes := False;
          for i10 := CLeft - 2 downto StartPOS do
          begin
            if TNodes[i10] + (CLeft - i10) > Length(TNodes) - 1 then
            begin
              Continue;
            end
            else
            begin
              if OneChangeTNodes = False then
              begin
                TNodes[i10] := TNodes[i10] + 1;
                OneChangeTNodes := True;
                for i11 := i10 + 1 to CLeft - 1 do
                begin
                  if TNodes[i11 - 1] + 1 > Length(TNodes) - 1 then
                  begin
                    Stop := True;
                    raise Exception.Create('');
                  end;
                  TNodes[i11] := TNodes[i11 - 1] + 1;
                end;
              end;
            end;
          end;
        except
        end;
      end
      else
      begin
        TNodes[CLeft - 1] := TNodes[CLeft - 1] + 1;
        if Length(TNodes) = 2 then
        begin
          Costil := Costil + 1;
          if Costil = 1 then
            Stop := True;
        end;
      end;
      if Stop = True then
        Break;
      i3 := 0;
      TI := 0;
      for i9 := 0 to MCountCircle - 1 do
      begin
        if i3 > CLeft - 1 then
        begin
          TNodes[TI + AverageCount] := i9;
          TI := TI + 1;
        end
        else
        if TNodes[i3] = i9 then
        begin
          i3 := i3 + 1;
        end
        else
        begin
          TNodes[TI + AverageCount] := i9;
          TI := TI + 1;
        end;
      end;
    end
    else
    begin
      IsMoving := False;
      if TNodes[CLeft - 1] + 1 > Length(TNodes) - 1 then
      begin
        try
          if TNodes[CLeft - 1 - TI2] + 1 > Length(TNodes) - 1 - TI2 then
          begin
            if CLeft - 1 - TI2 <= StartPOS then
            begin
              Stop := True;
              raise Exception.Create('');
            end;
            TI2 := TI2 + 1;
          end;
          OneChangeTNodes := False;
          for i10 := CLeft - 2 downto StartPOS do
          begin
            if TNodes[i10] + (CLeft - i10) > Length(TNodes) - 1 then
            begin
              Continue;
            end
            else
            begin
              if OneChangeTNodes = False then
              begin
                TNodes[i10] := TNodes[i10] + 1;
                OneChangeTNodes := True;
                for i11 := i10 + 1 to CLeft - 1 do
                begin
                  if TNodes[i11 - 1] + 1 > Length(TNodes) - 1 then
                  begin
                    Stop := True;
                    raise Exception.Create('');
                  end;
                  TNodes[i11] := TNodes[i11 - 1] + 1;
                end;
              end;
            end;
          end;
        except
        end;
      end
      else
      begin
        TNodes[CLeft - 1] := TNodes[CLeft - 1] + 1;
        if Length(TNodes) = 2 then
        begin
          Costil := Costil + 1;
          if Costil = 1 then
            Stop := True;
        end;
      end;
      if Stop = True then
        Break;
      i3 := 0;
      TI := 0;
      for i9 := 0 to MCountCircle - 1 do
      begin
        if i3 > CLeft - 1 then
        begin
          TNodes[TI + AverageCount] := i9;
          TI := TI + 1;
        end
        else
        if TNodes[i3] = i9 then
        begin
          i3 := i3 + 1;
        end
        else
        begin
          TNodes[TI + AverageCount] := i9;
          TI := TI + 1;
        end;
      end;
    end;
  end;
  if (MaxBandwidth <> MinBandwidth) then
  begin
    for i4 := 0 to MCountCircle - 1 do
    begin
      if i4 = AverageCount then
        TempStr := TempStr + ' |';
      TempStr := TempStr + ' ' + (IntToStr(MaxBandwidthNodes[i4]));
    end;
    FDataList.Add(
      'Наибольшую пропускную способность имеет -');
    FDataList.Add(TempStr + ' - Пропускная способность = ' +
      IntToStr(MaxBandwidth));
    TempStr := '';
    for i4 := 0 to MCountCircle - 1 do
    begin
      if i4 = AverageCount then
        TempStr := TempStr + ' |';
      TempStr := TempStr + ' ' + (IntToStr(MinBandwidthNodes[i4]));
    end;
    FDataList.Add(
      'Наименьшую пропускную способность имеет -');
    FDataList.Add(TempStr + ' - Пропускная способность = ' +
      IntToStr(MinBandwidth));
    TempStr := '';
  end
  else
    FDataList.Add(TempStr + 'Пропускная способность сети = ' +
      IntToStr(MaxBandwidth));
  FDataList.Strings[1] := 'Всего бисекций: ' + IntToStr(AllVariantsTrue);
  if CoFData = 0 then
  begin
    Memo1.Text := 'Нет данных для вывода.';
    FreeAndNil(FDataList);
    Exit;
  end;
  Button3.Enabled := True;
  CanvaIsChanged := False;
  Memo1.Text := FDataList.Text;
  FreeAndNil(FDataList);
end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  if CallWarningMessage = False then
    CloseAction := caNone;
end;

procedure TForm1.MenuItem1Click(Sender: TObject);
begin
  CallHelp;
end;

procedure TForm1.MenuItem6Click(Sender: TObject);
begin
  if CallWarningMessage = True then
    ClearAllData;
end;

function TForm1.FileOpenBNF(Path: WideString): boolean;
var
  TextFile: TFileStream;
  Data: TStringList;
  i, i1: integer;
  TOffset: integer = 0;
  ConnectedLinesIds: array of integer;
  EndLine: integer = 0;
  x, y, rad, x1, y1, x2, y2, RadTargetCircle, Id, IdStartCircle,
  IdEndCircle, Data1, Data2, Data3, CountConnectedLines: integer;
begin
  if (not Assigned(Form3)) then
    Form3 := TForm3.Create(Self);
  try
    ClearAllData;
    Data := TStringList.Create;
    TextFile := TFileStream.Create(Path, fmOpenRead);
    Data.LoadFromStream(TextFile);
    CountCircle := StrToInt(Data.Strings[0]);
    CountLine := StrToInt(Data.Strings[1]);
    SetLength(Circls, CountCircle);
    SetLength(Lines, CountLine);
    for i := 0 to CountCircle - 1 do
    begin
      Application.ProcessMessages;
      x := StrToInt(Data.Strings[2 + (5 * i) + TOffset]);
      y := StrToInt(Data.Strings[3 + (5 * i) + TOffset]);
      rad := StrToInt(Data.Strings[4 + (5 * i) + TOffset]);
      Id := StrToInt(Data.Strings[5 + (5 * i) + TOffset]);
      CountConnectedLines := StrToInt(Data.Strings[6 + (5 * i) + TOffset]);
      SetLength(ConnectedLinesIds, CountConnectedLines);
      for i1 := 0 to CountConnectedLines - 1 do
      begin
        Application.ProcessMessages;
        ConnectedLinesIds[i1] := StrToInt(Data.Strings[7 + (5 * i) + TOffset]);
        TOffset := TOffset + 1;
      end;
      Circls[i] := TCircle.Create(x, y, rad, id, IntToStr(id + 1), colBlue, PageGraph);
      Circls[i].CountConnectedLines := CountConnectedLines;
      Circls[i].ConnectedLinesIds := ConnectedLinesIds;
    end;
    EndLine := 6 + (5 * (CountCircle - 1)) + TOffset;
    for i := 0 to CountLine - 1 do
    begin
      Application.ProcessMessages;
      x1 := StrToInt(Data.Strings[EndLine + 1 + (11 * i)]);
      y1 := StrToInt(Data.Strings[EndLine + 2 + (11 * i)]);
      x2 := StrToInt(Data.Strings[EndLine + 3 + (11 * i)]);
      y2 := StrToInt(Data.Strings[EndLine + 4 + (11 * i)]);
      RadTargetCircle := StrToInt(Data.Strings[EndLine + 5 + (11 * i)]);
      Id := StrToInt(Data.Strings[EndLine + 6 + (11 * i)]);
      IdStartCircle := StrToInt(Data.Strings[EndLine + 7 + (11 * i)]);
      IdEndCircle := StrToInt(Data.Strings[EndLine + 8 + (11 * i)]);
      Data1 := StrToInt(Data.Strings[EndLine + 9 + (11 * i)]);
      Data2 := StrToInt(Data.Strings[EndLine + 10 + (11 * i)]);
      Data3 := StrToInt(Data.Strings[EndLine + 11 + (11 * i)]);
      Lines[i] := TLine.Create(x1, y1, x2, y2, Data1, Data2, Data3,
        Id, IdStartCircle, IdEndCircle, RadTargetCircle, PageGraph);
    end;
  except
    FreeAndNil(Data);
    FreeAndNil(TextFile);
    Form3.FreeCircleAndLineAndClear;
    CanvaIsChangedFile := False;
    Result := False;
    Exit;
  end;
  FreeAndNil(Data);
  FreeAndNil(TextFile);
  GWidth := Form3.Image1.Width;
  GHeight := Form3.Image1.Height;
  Memo1.Clear;
  CanvaIsEmpty := False;
  CanvaIsChangedFile := False;
  CanvaIsChanged := False;
  CanvasChanged;
  Form3.OutputImage;
  if CountCircle = 0 then
    Button1.Enabled := False;
  Result := True;
end;

function TForm1.FileSaveAsBNF(Path: WideString): boolean;
var
  TextFile: TFileStream;
  Data: TStringList;
  i, i1: integer;
begin
  try
    Data := TStringList.Create;
    Data.Add(IntToStr(CountCircle));
    Data.Add(IntToStr(CountLine));
    for i := 0 to CountCircle - 1 do
    begin
      Application.ProcessMessages;
      Data.Add(IntToStr(Circls[i].x));
      Data.Add(IntToStr(Circls[i].y));
      Data.Add(IntToStr(Circls[i].rad));
      Data.Add(IntToStr(Circls[i].Id));
      Data.Add(IntToStr(Circls[i].CountConnectedLines));
      for i1 := 0 to Circls[i].CountConnectedLines - 1 do
      begin
        Application.ProcessMessages;
        Data.Add(IntToStr(Circls[i].ConnectedLinesIds[i1]));
      end;
    end;
    for i := 0 to CountLine - 1 do
    begin
      Application.ProcessMessages;
      Data.Add(IntToStr(Lines[i].x1));
      Data.Add(IntToStr(Lines[i].y1));
      Data.Add(IntToStr(Lines[i].x2));
      Data.Add(IntToStr(Lines[i].y2));
      Data.Add(IntToStr(Lines[i].RadTargetCircle));
      Data.Add(IntToStr(Lines[i].Id));
      Data.Add(IntToStr(Lines[i].IdStartCircle));
      Data.Add(IntToStr(Lines[i].IdEndCircle));
      Data.Add(IntToStr(Lines[i].Data1));
      Data.Add(IntToStr(Lines[i].Data2));
      Data.Add(IntToStr(Lines[i].Data3));
    end;
    TextFile := TFileStream.Create(Path, fmCreate);
    Data.SaveToStream(TextFile);
  except
    FreeAndNil(Data);
    FreeAndNil(TextFile);
    Result := False;
    Exit;
  end;
  FreeAndNil(Data);
  FreeAndNil(TextFile);
  CanvaIsChangedFile := False;
  Result := True;
end;

procedure TForm1.CallFileOpenDialog;
begin
  if (OpenDialog1.Execute = True) then
  begin
    if (CallWarningMessage = False) then
      Exit;
    if (FileOpenBNF(OpenDialog1.FileName)) = False then
    begin
      MessageDlg(Application.Title, 'Не удалось открыть файл.',
        mtError, [mbOK], 0);
      GSavePath := '';
      Exit;
    end;
    GSavePath := OpenDialog1.FileName;
  end
  else
    Exit;
end;

procedure TForm1.CallFileSaveAsDialog;
begin
  if (SaveDialog1.Execute = True) then
  begin
    SaveDialog1.FileName := ChangeFileExt(SaveDialog1.FileName, '');
    SaveDialog1.FileName := SaveDialog1.FileName + '.bnf';
    if (FileSaveAsBNF(SaveDialog1.FileName)) = False then
    begin
      MessageDlg(Application.Title, 'Не удалось сохранить файл.',
        mtError, [mbOK], 0);
      GSavePath := '';
      Exit;
    end;
    GSavePath := SaveDialog1.FileName;
  end
  else
    Exit;
end;

procedure TForm1.CallHelp;
begin
  MessageDlg(Application.Title, 'Текущая версия программы: ' +
    ProgramVersion + LineEnding +
    'Разработчики: Т.З. Аралбаев, Р.Р. Галимов,' +
    LineEnding + 'Д.С. Гусельников.',
    mtInformation, [mbOK], 0);
end;

procedure TForm1.CanvasChanged;
begin
  if (CanvaIsChanged = True) then
  begin
    Button3.Enabled := False;
    Memo1.Clear;
  end;
  if (CanvaIsEmpty = True) then
  begin
    Button4.Visible := True;
    Button2.Visible := True;
    Button7.Visible := True;
    Button8.Visible := True;
    Button5.Visible := False;
    Button6.Visible := False;
    Button14.Visible := False;
    Button15.Visible := False;
    Button1.Enabled := False;
    Memo1.Clear;
    ClearTable;
    ClearPreviewGraph;
    Exit;
  end;
  Button4.Visible := False;
  Button2.Visible := False;
  Button7.Visible := False;
  Button8.Visible := False;
  Button5.Visible := True;
  Button6.Visible := True;
  Button14.Visible := True;
  Button15.Visible := True;
  Button1.Enabled := True;
  DrawPreviewGraph;
  FillTable;
end;

procedure TForm1.CallGraphDraw;
begin
  if (Assigned(Form5)) then
    if (Form5.LiveForma = True) then
      Form5.Close;
  if (not Assigned(Form3)) then
    Form3 := TForm3.Create(Self);
  Form3.ShowModal;
  CanvasChanged;
end;

procedure TForm1.CallViewGraph;
begin
  if (not Assigned(Form5)) then
    Form5 := TForm5.Create(Self)
  else
  begin
    FreeAndNil(Form5);
    Form5 := TForm5.Create(Self);
  end;
  Form5.Show;
end;

procedure TForm1.MenuItem10Click(Sender: TObject);
begin
  if CallWarningMessage = True then
    Application.Terminate;
end;

procedure TForm1.ClearPreviewGraph;
begin
  Image1.Canvas.Brush.Color := clWhite;
  Image1.Canvas.FillRect(Image1.Canvas.ClipRect);
  Image4.Canvas.Brush.Color := clWhite;
  Image4.Canvas.FillRect(Image4.Canvas.ClipRect);
end;

procedure TForm1.MarkingTable;
var
  i: integer = 0;
  j: integer = 0;
  CountNetworkNodes: integer = 0;
begin
  if (not Assigned(Form3)) then
    CountNetworkNodes := 10
  else
  begin
    if (CanvaIsEmpty = True) then
      CountNetworkNodes := 10
    else
      CountNetworkNodes := CountCircle;
  end;
  if (CountNetworkNodes >= 0) then
  begin
    StringGrid1.RowCount := CountNetworkNodes + 1;
    StringGrid1.ColCount := CountNetworkNodes + 1;
    StringGrid4.RowCount := CountNetworkNodes + 1;
    StringGrid4.ColCount := CountNetworkNodes + 1;
    for i := 1 to (StringGrid1.RowCount - 1) do
    begin
      StringGrid1.Cells[i, 0] := IntToStr(i);
      StringGrid1.Cells[0, i] := IntToStr(i);
      StringGrid4.Cells[i, 0] := IntToStr(i);
      StringGrid4.Cells[0, i] := IntToStr(i);
    end;
    if (CountNetworkNodes >= 1) then
      for i := 1 to (StringGrid1.RowCount - 1) do
        for j := 1 to (StringGrid1.ColCount - 1) do
        begin
          if (StringGrid1.Cells[j, i] <> '1') then
          begin
            StringGrid1.Cells[j, i] := '0';
            StringGrid4.Cells[j, i] := 'X';
          end;
        end;
  end;
end;

procedure TForm1.DrawPreviewGraph;
begin
  ClearPreviewGraph;
  DrawFPVectorialToCanvas(PageGraph,
    Image1.Canvas, 0, 0, Image1.Width / Form3.Image1.Width, Image1.Height /
    Form3.Image1.Height);
  DrawFPVectorialToCanvas(PageGraph,
    Image4.Canvas, 0, 0, Image4.Width / Form3.Image1.Width, Image4.Height /
    Form3.Image1.Height);
end;

procedure TForm1.ClearTable;
begin
  StringGrid1.Clean;
  StringGrid4.Clean;
  MarkingTable;
end;

procedure TForm1.FillTable;
var
  i: integer = 0;
begin
  ClearTable;
  for i := 0 to CountLine - 1 do
  begin
    if (Lines[i].IdStartCircle > Lines[i].IdEndCircle) then
    begin
      StringGrid1.Cells[Lines[i].IdStartCircle + 1, Lines[i].IdEndCircle + 1] :=
        '1';
      //   StringGrid1.Cells[Lines[i].IdEndCircle + 1, Lines[i].IdStartCircle + 1] :=
      //     '1';
      StringGrid4.Cells[Lines[i].IdStartCircle + 1, Lines[i].IdEndCircle + 1] :=
        IntToStr(Lines[i].Data3);
    end
    else
    begin
      StringGrid1.Cells[Lines[i].IdEndCircle + 1, Lines[i].IdStartCircle + 1] :=
        '1';
      //   StringGrid1.Cells[Lines[i].IdStartCircle + 1, Lines[i].IdEndCircle + 1] :=
      //     '1';
      StringGrid4.Cells[Lines[i].IdEndCircle + 1, Lines[i].IdStartCircle + 1] :=
        IntToStr(Lines[i].Data3);
    end;
  end;
end;

procedure TForm1.MenuItem7Click(Sender: TObject);
begin
  CallFileOpenDialog;
end;

procedure TForm1.MenuItem8Click(Sender: TObject);
begin
  if (GSavePath <> '') then
  begin
    if (FileSaveAsBNF(GSavePath)) = False then
    begin
      MessageDlg(Application.Title, 'Не удалось сохранить файл.',
        mtError, [mbOK], 0);
      Exit;
    end;
  end
  else
    CallFileSaveAsDialog;
end;

procedure TForm1.MenuItem9Click(Sender: TObject);
begin
  CallFileSaveAsDialog;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  FindingOfBandwidth;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  Rank;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  CallGraphDraw;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  CallGraphDraw;
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  CallViewGraph;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  MarkingTable;
  ClearPreviewGraph;
end;

end.
