﻿unit GraphGUI;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus,
  ExtCtrls, UserDialogInputDate, CreateGraph, DefinesConst,
  fpvectorialM, fpvtocanvasM, FPimage;

type

  { TForm3 }

  TForm3 = class(TForm)
    Image1: TImage;
    MainMenu1: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    PopupMenu1: TPopupMenu;
    function CreateAndShowUserDialog: integer;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormHide(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure UpdateCanvas;
    procedure FreeCircleAndLineAndClear;
    procedure InitializationComponent;
    procedure ChangeLengthArrays;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    function Selected(X, Y: integer): boolean;
    function WithoutDuplicationCircle(X, Y: integer): boolean;
    procedure ImageClear;
    procedure OutputImage;
    function FindLocationArrayCircle: integer;
    function FindLocationArrayLine: integer;
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form3: TForm3;
  Form4: TForm4;
  CountCircle: integer = 0;
  CountLine: integer = 0;
  Circls: array of TCircle;
  Lines: array of TLine;
  Selecting: boolean = False;
  CanvaIsEmpty: boolean = True;
  CanvaIsChanged: boolean = False;
  CanvaIsChangedFile: boolean = False;
  MainGraph: TvVectorialDocument;
  PageGraph: TvVectorialPage;
  GWidth, GHeight, GX, GY: integer;

implementation

{$R *.lfm}

{ TForm3 }

function TForm3.FindLocationArrayCircle: integer;
var
  i: integer;
begin
  for i := 0 to High(Circls) do
    if Circls[i] = nil then
      Break;
  Result := i;
end;

function TForm3.FindLocationArrayLine: integer;
var
  i: integer;
begin
  for i := 0 to High(Lines) do
    if Lines[i] = nil then
      Break;
  Result := i;
end;

procedure TForm3.ImageClear;
begin
  Image1.Canvas.Brush.Color := clWhite;
  Image1.Canvas.FillRect(Image1.Canvas.ClipRect);
end;

procedure TForm3.OutputImage;
begin
  ImageClear;
  DrawFPVectorialToCanvas(PageGraph, Image1.Canvas);
end;

procedure TForm3.InitializationComponent;
begin
  ImageClear;
  MainGraph := TvVectorialDocument.Create;
  PageGraph := MainGraph.AddPage();
  PageGraph.Width := Image1.Width;
  PageGraph.Height := Image1.Height;
end;

procedure TForm3.ChangeLengthArrays;
begin
  if (Length(Circls) = CountCircle) then
    SetLength(Circls, Length(Circls) + 1);
  if (Length(Lines) = CountLine) then
    SetLength(Lines, Length(Lines) + 1);
end;

function TForm3.CreateAndShowUserDialog: integer;
begin
  if (not Assigned(Form4)) then
    Form4 := TForm4.Create(Form3)
  else
  begin
    FreeAndNil(Form4);
    Form4 := TForm4.Create(Form3);
  end;
  Result := Form4.ShowModal;
end;

procedure TForm3.FormClose(Sender: TObject; var CloseAction: TCloseAction);
var
  i, i1, i2, i3: integer;
  Found, Found2: boolean;
begin
  for i2 := 0 to High(Circls) do
    if Circls[i2] = nil then
      Break;
  Found2 := False;
  for i3 := i2 + 1 to High(Circls) do
    if Circls[i3] <> nil then
    begin
      Found2 := True;
      Break;
    end;
  if Found2 = True then
  begin
    MessageDlg(Application.Title, 'Не найден узел ' +
      IntToStr(i2 + 1) + '.',
      mtError, [mbOK], 0);
    CloseAction := caNone;
    Exit;
  end;
  for i := 0 to High(Circls) do
    if Circls[i] <> nil then
      if Circls[i].CountConnectedLines = 0 then
        Break;
  Found := False;
  for i1 := i + 1 to High(Circls) do
  begin
    if Circls[i1] <> nil then
      if Circls[i1].CountConnectedLines <> 0 then
      begin
        Found := True;
        Break;
      end;
  end;
  if Found = True then
  begin
    MessageDlg(Application.Title, 'Узел ' + IntToStr(i + 1) +
      ' не связан с другими узлами сети.',
      mtError, [mbOK], 0);
    CloseAction := caNone;
    Exit;
  end;
end;

procedure TForm3.FormHide(Sender: TObject);
var
  i1: integer;
begin
  GWidth := Image1.Width;
  GHeight := Image1.Height;
  if (Selecting = True) then
  begin
    for i1 := 0 to High(Circls) do
      if Circls[i1] <> nil then
        if (Circls[i1].Selected = True) then
          Break;
    Selected(Circls[i1].x, Circls[i1].y);
  end;
end;

procedure TForm3.MenuItem3Click(Sender: TObject);
var
  i, i1, i2: integer;
  Found: boolean = False;
begin
  for i := 0 to High(Circls) do
    if Circls[i] <> nil then
      if Circls[i].FindMe(GX, GY) = True then
        Break;
  FreeAndNil(Circls[i]);
  for i2 := 0 to High(Circls) do
    if Circls[i2] = nil then
    begin
      Break;
    end;
  for i1 := i2 + 1 to High(Circls) do
    if Circls[i1] <> nil then
    begin
      Found := True;
    end;
  if Found = False then
    SetLength(Circls, i2 + 1);
  CountCircle := CountCircle - 1;
  Selecting := False;
  PageGraph.Clear;
  if CountCircle = 0 then
  begin
    FreeCircleAndLineAndClear;
    MenuItem5.Enabled := False;
    OutputImage;
  end
  else
  begin
    CanvaIsChanged := True;
    CanvaIsChangedFile := True;
    UpdateCanvas;
    OutputImage;
  end;
end;

procedure TForm3.MenuItem4Click(Sender: TObject);
begin
  FreeCircleAndLineAndClear;
  MenuItem5.Enabled := False;
end;

procedure TForm3.MenuItem5Click(Sender: TObject);
begin
  ModalResult := mrClose;
end;

function TForm3.Selected(X, Y: integer): boolean;
var
  i, i1, i2, temp: integer;
  LineFound: boolean = False;
begin
  for i := 0 to High(Circls) do
  begin
    if Circls[i] <> nil then
      if (Circls[i].FindMe(X, Y)) = True then
      begin
        if (Selecting = True) then
        begin
          Selecting := False;
          for i1 := 0 to High(Circls) do
            if Circls[i1] <> nil then
              if (Circls[i1].Selected = True) then
                Break;
          Circls[i1].Selected := False;
          if (Circls[i1].x = Circls[i].x) and (Circls[i1].y = Circls[i].y) then
          begin
            Circls[i].Draw(Circls[i].MainColor);
            Result := True;
            OutputImage;
            Exit;
          end;
          for i2 := 0 to High(Lines) do
            if Lines[i2] <> nil then
              if (Lines[i2].FindMe(Circls[i1].x, Circls[i1].y, Circls[i].x,
                Circls[i].y) = True) then
              begin
                LineFound := True;
                Break;
              end;
          if (LineFound = False) then
          begin
            if (CreateAndShowUserDialog <> mrOk) then
            begin
              Circls[i1].Draw(Circls[i1].MainColor);
              Circls[i].Draw(Circls[i].MainColor);
              OutputImage;
              Result := True;
              Exit;
            end;
            CanvaIsChanged := True;
            CanvaIsChangedFile := True;
            temp := FindLocationArrayLine;
            Lines[FindLocationArrayLine] :=
              TLine.Create(Circls[i1].x, Circls[i1].y, Circls[i].x,
              Circls[i].y, Data1, Data2, Data3, FindLocationArrayLine,
              Circls[i1].Id, Circls[i].Id, Circls[i].rad, Circls[i].Canva);
            SetLength(Circls[i1].ConnectedLinesIds,
              Length(Circls[i1].ConnectedLinesIds) + 1);
            Circls[i1].ConnectedLinesIds[Circls[i1].CountConnectedLines] := temp;
            Circls[i1].CountConnectedLines := Circls[i1].CountConnectedLines + 1;
            SetLength(Circls[i].ConnectedLinesIds,
              Length(Circls[i].ConnectedLinesIds) + 1);
            Circls[i].ConnectedLinesIds[Circls[i].CountConnectedLines] := temp;
            Circls[i].CountConnectedLines := Circls[i].CountConnectedLines + 1;
            CountLine := CountLine + 1;
          end
          else
          begin
            temp := FindLocationArrayLine;
            Lines[FindLocationArrayLine] :=
              TLine.Create(Circls[i1].x, Circls[i1].y, Circls[i].x,
              Circls[i].y, Lines[i2].Data1, Lines[i2].Data2, Lines[i2].Data3,
              FindLocationArrayLine, Circls[i1].Id, Circls[i].Id,
              Circls[i].rad, Circls[i].Canva);
            SetLength(Circls[i1].ConnectedLinesIds,
              Length(Circls[i1].ConnectedLinesIds) + 1);
            Circls[i1].ConnectedLinesIds[Circls[i1].CountConnectedLines] := temp;
            Circls[i1].CountConnectedLines := Circls[i1].CountConnectedLines + 1;
            SetLength(Circls[i].ConnectedLinesIds,
              Length(Circls[i].ConnectedLinesIds) + 1);
            Circls[i].ConnectedLinesIds[Circls[i].CountConnectedLines] := temp;
            Circls[i].CountConnectedLines := Circls[i].CountConnectedLines + 1;
            CountLine := CountLine + 1;
          end;
          Circls[i1].Draw(Circls[i1].MainColor);
          Circls[i].Draw(Circls[i].MainColor);
          Result := True;
          OutputImage;
          Exit;
        end;
        Circls[i].Select;
        Selecting := True;
        Result := True;
        OutputImage;
        Exit;
      end;
  end;
  Result := False;
end;

function TForm3.WithoutDuplicationCircle(X, Y: integer): boolean;
var
  i: integer;
begin
  for i := 0 to High(Circls) do
    if Circls[i] <> nil then
    begin
      if ((Circls[i].x - Circls[i].rad * 2 - 8 < X) and
        (Circls[i].x + Circls[i].rad * 2 + 8 > X)) and
        ((Circls[i].y - Circls[i].rad * 2 - 8 < Y) and
        (Circls[i].y + Circls[i].rad * 2 + 8 > Y)) then
      begin
        Result := True;
        Exit;
      end;
    end;
  Result := False;
end;

procedure TForm3.Image1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
var
  i: integer;
begin
  if (Button <> mbLeft) then
  begin
    for i := 0 to High(Circls) do
      if Circls[i] <> nil then
        if Circls[i].FindMe(x, y) = True then
        begin
          if Circls[i].CountConnectedLines = 0 then
          begin
            GX := X;
            GY := Y;
            PopupMenu1.PopUp;
            Break;
          end;
        end;
    Exit;
  end;
  ChangeLengthArrays;
  if (Selected(X, Y) = False) then
  begin
    if (WithoutDuplicationCircle(X, Y) = True) then
      Exit;
    CanvaIsChanged := True;
    CanvaIsChangedFile := True;
    Circls[FindLocationArrayCircle] :=
      TCircle.Create(X, Y, 23, FindLocationArrayCircle,
      IntToStr(FindLocationArrayCircle + 1), colBlue, PageGraph);
    CountCircle := CountCircle + 1;
    CanvaIsEmpty := False;
    MenuItem5.Enabled := True;
    OutputImage;
  end;
end;

procedure TForm3.FormCreate(Sender: TObject);
begin
  InitializationComponent;
end;

procedure TForm3.FormDestroy(Sender: TObject);
begin
  FreeCircleAndLineAndClear;
  FreeAndNil(Form4);
  FreeAndNil(MainGraph);
end;

procedure TForm3.UpdateCanvas;
var
  i: integer;
begin
  for i := 0 to High(Lines) do
  begin
    if Lines[i] <> nil then
      Lines[i].DrawLine;
  end;
  for i := 0 to High(Circls) do
  begin
    if Circls[i] <> nil then
      Circls[i].Draw(Circls[i].MainColor);
  end;
end;

procedure TForm3.FreeCircleAndLineAndClear;
var
  i: integer;
begin
  for i := 0 to High(Lines) do
    FreeAndNil(Lines[i]);
  for i := 0 to High(Circls) do
    FreeAndNil(Circls[i]);
  SetLength(Circls, 0);
  SetLength(Lines, 0);
  CountCircle := 0;
  CountLine := 0;
  CanvaIsChanged := True;
  CanvaIsChangedFile := True;
  ImageClear;
  PageGraph.Clear;
  CanvaIsEmpty := True;
  Selecting := False;
end;

end.
