﻿unit CreateGraph;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Math, fpvectorialM, FPimage;

type

  { TCircle }

  TCircle = class
    x: integer;
    y: integer;
    rad: integer;
    Id: integer;
    Text: WideString;
    CountConnectedLines: integer;
    ConnectedLinesIds: array of integer;
    MainColor: TFPColor;
    Canva: TvVectorialPage;
    Selected: boolean;
  public
    constructor Create(Tx, Ty, Trad, TId: integer; TText: WideString;
      MainColorCircle: TFPColor; TCanva: TvVectorialPage);
    procedure Draw(Color: TFPColor);
    procedure DrawText;
    function FindMe(Tx, Ty: integer): boolean;
    procedure Select;
  end;

  { TLine }

  TLine = class
    x1: integer;
    y1: integer;
    x2: integer;
    y2: integer;
    Data1: integer;
    Data2: integer;
    Data3: integer;
    Id: integer;
    IdStartCircle: integer;
    IdEndCircle: integer;
    RadTargetCircle: integer;
    Canva: TvVectorialPage;
  public
    constructor Create(Tx1, Ty1, Tx2, Ty2, TData1, TData2, TData3,
      TId, TIdStartCircle, TIdEndCircle, TRadTargetCircle: integer;
      TCanva: TvVectorialPage);
    procedure DrawLine;
    function FindMe(Tx1, Ty1, Tx2, Ty2: integer): boolean;
    procedure DrawText;
    procedure DrawArrow;
  end;

implementation

{ TCircle }

constructor TCircle.Create(Tx, Ty, Trad, TId: integer; TText: WideString;
  MainColorCircle: TFPColor; TCanva: TvVectorialPage);
begin
  x := Tx;
  y := Ty;
  rad := Trad;
  Id := TId;
  Text := TText;
  MainColor := MainColorCircle;
  Canva := TCanva;
  Draw(MainColor);
end;

procedure TCircle.Draw(Color: TFPColor);
begin
  Canva.AddCircle(x, y, rad, 3, Color);
  DrawText;
end;

procedure TCircle.DrawText;
var
  TempX, TempY: integer;
begin
  TempX := round(rad / 3);
  TempY := round(rad / 2);
  Canva.AddText(x - TempX, y - TempY, 0, 'default', 12, Text);
end;

procedure TCircle.Select;
begin
  Selected := True;
  Draw(colRed);
end;

function TCircle.FindMe(Tx, Ty: integer): boolean;
begin
  if ((x - rad < Tx) and (x + rad > Tx)) and ((y - rad < Ty) and (y + rad > Ty)) then
    Result := True
  else
    Result := False;
end;

{ TLine }

constructor TLine.Create(Tx1, Ty1, Tx2, Ty2, TData1, TData2, TData3,
  TId, TIdStartCircle, TIdEndCircle, TRadTargetCircle: integer; TCanva: TvVectorialPage);
begin
  x1 := Tx1;
  y1 := Ty1;
  x2 := Tx2;
  y2 := Ty2;
  Data1 := TData1;
  Data2 := TData2;
  Data3 := TData3;
  Id := TId;
  IdStartCircle := TIdStartCircle;
  IdEndCircle := TIdEndCircle;
  RadTargetCircle := TRadTargetCircle;
  Canva := TCanva;
  DrawLine;
end;

procedure TLine.DrawLine;
var
  Nx2, Ny2, Nx3, Ny3: integer;
  Distance, Angle, Angle2: double;
begin
  Distance := (sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) - RadTargetCircle);
  Angle := radtograd(ArcTan2(y1 - y2, x1 - x2));
  Angle2 := radtograd(ArcTan2(y2 - y1, x2 - x1));
  Nx2 := x2 + Round(Distance * (cos(gradtorad(Angle))));
  Ny2 := y2 + Round(Distance * (sin(gradtorad(Angle))));
  Nx3 := x1 + Round(Distance * (cos(gradtorad(Angle2))));
  Ny3 := y1 + Round(Distance * (sin(gradtorad(Angle2))));
  Canva.StartPath(Nx2, Ny2);
  Canva.AddLineToPath(Nx3, Ny3);
  Canva.SetPenWidth(3);
  Canva.EndPath();
  DrawText;
end;

procedure TLine.DrawArrow;
var
  x3, y3, x4, y4, Nx3, Ny3: integer;
  Distance, Angle2: double;
begin
  Distance := (sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) - RadTargetCircle);
  Angle2 := radtograd(ArcTan2(y2 - y1, x2 - x1));
  Nx3 := x1 + Round(Distance * (cos(gradtorad(Angle2))));
  Ny3 := y1 + Round(Distance * (sin(gradtorad(Angle2))));
  x4 := Nx3 + Round(22 * cos(gradtorad(Angle2 - 160)));
  y4 := Ny3 + Round(22 * sin(gradtorad(Angle2 - 160)));
  x3 := Nx3 + Round(22 * cos(gradtorad(Angle2 + 160)));
  y3 := Ny3 + Round(22 * sin(gradtorad(Angle2 + 160)));
  Canva.StartPath(Nx3, Ny3);
  Canva.AddLineToPath(x3, y3);
  Canva.SetPenWidth(3);
  Canva.EndPath();
  Canva.StartPath(Nx3, Ny3);
  Canva.AddLineToPath(x4, y4);
  Canva.SetPenWidth(3);
  Canva.EndPath();
end;

procedure TLine.DrawText;
var
  MidX, MidY: integer;
begin
  MidX := round(((x1 + x2) / 2));
  MidY := round(((y1 + y2) / 2));
  Canva.AddText(MidX - 15, MidY - 35, 0, 'default', 12, IntToStr(Data3), colDkGreen);
  //  Canva.AddText(MidX - 15, MidY + 5, 0, 'default', 12,
  //    IntToStr(Data1) + ' | ' + IntToStr(Data2), colRed);
end;

function TLine.FindMe(Tx1, Ty1, Tx2, Ty2: integer): boolean;
begin
  if ((Tx1 = x2) and (Ty1 = y2) and (Tx2 = x1) and (Ty2 = y1) or
    (Tx1 = x1) and (Ty1 = y1) and (Tx2 = x2) and (Ty2 = y2)) then
    Result := True
  else
    Result := False;
end;

end.
