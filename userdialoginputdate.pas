﻿unit UserDialogInputDate;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, DefinesConst;

type

  { TForm4 }

  TForm4 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Edit3: TEdit;
    Label3: TLabel;
    procedure Edit3EditingDone(Sender: TObject);
    procedure Edit3KeyPress(Sender: TObject; var Key: char);
    procedure InputCheck(EditNumber: integer);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form4: TForm4;
  Data1, Data2, Data3: integer;

implementation

{$R *.lfm}

{ TForm4 }

procedure TForm4.InputCheck(EditNumber: integer);
begin
  if (EditNumber = 3) or (EditNumber = 0) then
  begin
    if (Edit3.Text = '') and (EditNumber = 3) then
      Exit;
    if (Edit3.Text = '') and (EditNumber = 0) then
    begin
      MessageDlg(Application.Title, 'Введите данные.',
        mtInformation, [mbOK], 0);
      Exit;
    end;
    try
      Data3 := StrToInt(Edit3.Text);
      if (Data3 = 0) and (Length(Edit3.Text) > 1) then
        raise Exception.Create('');
    except
      Edit3.Text := '';
      MessageDlg(Application.Title, 'Некорректное значение.',
        mtWarning, [mbOK], 0);
      Exit;
    end;
    if (Data3 > MaxValueCellTable4) then
    begin
      MessageDlg(Application.Title,
        'Превышено максимально допустимое значение !'
        + LineEnding + 'Максимально допустимое значение: ' +
        IntToStr(MaxValueCellTable4),
        mtInformation, [mbOK], 0);
      Edit3.Text := IntToStr(MaxValueCellTable4);
      Exit;
    end;
    if (EditNumber = 3) then
      Exit;
  end;
  ModalResult := mrOk;
end;

procedure TForm4.Edit3KeyPress(Sender: TObject; var Key: char);
begin
  if not (Key in ['0'..'9', #8]) then                              //вводимых
    Key := #0;
end;

procedure TForm4.Edit3EditingDone(Sender: TObject);
begin
  InputCheck(3);
end;

procedure TForm4.Button1Click(Sender: TObject);
begin
  InputCheck(0);
end;

procedure TForm4.Button2Click(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
