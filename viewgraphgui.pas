﻿unit ViewGraphGUI;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, GraphGUI, fpvtocanvasM;

type

  { TForm5 }

  TForm5 = class(TForm)
    Image1: TImage;
    procedure ImageClear;
    procedure ChangingCanvas;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { private declarations }
  public
    LiveForma: boolean;
    { public declarations }
  end;

var
  Form5: TForm5;

implementation

{$R *.lfm}

{ TForm5 }

procedure TForm5.ImageClear;
begin
  Image1.Canvas.Brush.Color := clWhite;
  Image1.Canvas.FillRect(Image1.Canvas.ClipRect);
end;

procedure TForm5.ChangingCanvas;
begin
  while (Image1.Height >= Image1.Picture.Bitmap.Height) do
  begin
    Image1.Picture.Bitmap.Height := Image1.Picture.Bitmap.Height + 600;
  end;
  while (Image1.Width >= Image1.Picture.Bitmap.Width) do
  begin
    Image1.Picture.Bitmap.Width := Image1.Picture.Bitmap.Width + 600;
  end;
  while (Image1.Height + 900 <= Image1.Picture.Bitmap.Height) do
  begin
    Image1.Picture.Bitmap.Height := Image1.Picture.Bitmap.Height - 600;
  end;
  while (Image1.Width + 900 <= Image1.Picture.Bitmap.Width) do
  begin
    Image1.Picture.Bitmap.Width := Image1.Picture.Bitmap.Width - 600;
  end;
end;

procedure TForm5.FormResize(Sender: TObject);
begin
  ChangingCanvas;
  ImageClear;
  DrawFPVectorialToCanvas(GraphGUI.PageGraph,
    Image1.Canvas, 0, 0, Image1.Width / GraphGUI.GWidth, Image1.Height /
    GraphGUI.GHeight);
end;

procedure TForm5.FormCreate(Sender: TObject);
begin
  LiveForma := True;
end;

procedure TForm5.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  LiveForma := False;
end;

end.
